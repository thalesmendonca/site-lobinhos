
const url = "https://lobinhos.herokuapp.com/wolves/" 
const $ = document

/*Postando novos lobo na adoção*/

let form = $.querySelector("#add-wolf")
form.addEventListener("submit", e => {
    e.preventDefault()

    let name_wolf = $.querySelector("#name-input").value
    let age = $.querySelector("#age-input").value
    let image = $.querySelector("#image-input").value
    let description = $.querySelector("#description-input").value


    let fetchBody = {
        "wolf":{
            "name": name_wolf,
            "description": description,
            "link_image": image,
            "age": age
        }
    }

    let fetchConfig = {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(fetchBody)
    }

    fetch(url, fetchConfig)
    .then(() => {
        alert("Lobo adicionado a adoção com sucesso!")
    })
    .catch(() => {
        alert("Algo inesperado aconteceu, o lobo não foi adicionado a adoção.")
    })
    
    name_wolf.value = ""
    age.value = ""
    image.value = ""
    description.value = ""
})