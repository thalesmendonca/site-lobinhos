const $ = document
const url = "https://lobinhos.herokuapp.com/wolves/"


/*Fazendo o header diminuir ao scrollar a pagina*/
let header = $.querySelector("header");

$.addEventListener('scroll', function() {
    var distance = window.pageYOffset;
    if (distance > 100){
      header.classList.remove('large');
      header.classList.add('small');
    } else {
        header.classList.remove('small');
        header.classList.add('large');
    }
});

/*Botando lobos aleatorios da api para aparecerem na tela*/
function getRandom(max) {
    return Math.floor(Math.random() * max + 1)
}

let numeroAleatorio
fetch(url)
.then(data => data.json())
.then(resp => {
    let exemploUm = $.querySelector(".loboUm")

    /*MODIFICANOD O EXEMPLO UM*/
    numeroAleatorio = getRandom(resp.length)
    /*Modificando a imagem */
    let novoLoboUm = resp[numeroAleatorio]
    let imagemUm = $.querySelector("#imgLobo")
    imagemUm.setAttribute('src', novoLoboUm.link_image)
    /*Modificando o texto*/
    $.querySelector("#nomeLobo").innerHTML = novoLoboUm.name
    $.querySelector("#idLobo").innerHTML = "ID:" + novoLoboUm.id
})  