/*Função para conseguir o valor random pra exibição do lobo */
function getRandom(max) {
    return Math.floor(Math.random() * max + 1)
}

const url = "https://lobinhos.herokuapp.com/wolves/" 
const $ = document


let botaoAdotar = $.querySelector("#adotar")
let botaoExcluir = $.querySelector("#excluir")

/*Exibindo um lobo aleatorio na pagina */

let numeroAleatorio = getRandom(20)


function showLobo(indice) {
    fetch(url)
    .then(data => data.json())
    .then(resp => {
    
        /*Modificando a imagem */
        let novoLobo = resp[indice]
        let imagemLobo = $.querySelector("#imagem-lobo")
        imagemLobo.setAttribute('src', novoLobo.link_image)  
    
        /*Modificando o texto*/
        $.querySelector("#nome-lobo").innerHTML = novoLobo.name
        $.querySelector("#descrição1-lobo").innerHTML = novoLobo.description
        $.querySelector("#descrição2-lobo").innerHTML = novoLobo.description
        
        console.log(novoLobo)
        botaoExcluir.addEventListener("click", e => {
            e.preventDefault()
            var r = confirm("Tem certeza que deseja excluir este lobo?")
            if(r){
                let fetchConfig = {
                    method: "delete"
                } 
    
                fetch(url+novoLobo.id,fetchConfig)
                .then(() => {
    
                    console.log("Lobo apagado com sucesso")
                    location.reload()
                })  
            }
        })
    })   
}

showLobo(numeroAleatorio)