
/*import { showLobinho } from "./showLobinhojs";*/

const $ = document
const url = "https://lobinhos.herokuapp.com/wolves/"

/*Fazendo o header diminuir ao scrollar a pagina*/
let header = $.querySelector("header");

$.addEventListener('scroll', function() {
    var distance = window.pageYOffset;
    if (distance > 100){
      header.classList.remove('large');
      header.classList.add('small');
    } else {
        header.classList.remove('small');
        header.classList.add('large');
    }
});

/*Botando lobos aleatorios da api para aparecerem na tela*/

function getRandom(max) {
    return Math.floor(Math.random() * max + 1)
}

let numeroAleatorio
fetch(url)
.then(data => data.json())
.then(resp => {
    let exemploUm = $.querySelector(".loboUm")
    let exemploDois = $.querySelector(".loboDois")
    let exemploTres = $.querySelector(".loboTres")
    let exemploQuatro = $.querySelector(".loboQuatro")

    /*MODIFICANOD O EXEMPLO UM*/
    numeroAleatorio = getRandom(resp.length)
    /*Modificando a imagem */
    let novoLoboUm = resp[numeroAleatorio]
    let imagemUm = exemploUm.firstElementChild
    imagemUm.setAttribute('src', novoLoboUm.link_image)
    /*Modificando o texto*/
    $.querySelector("#nome").innerHTML = novoLoboUm.name
    $.querySelector("#idade").innerHTML = novoLoboUm.age
    $.querySelector("#descrição").innerHTML = novoLoboUm.description
    /*fazendo botao funcionar */
    /*let botao1 = $.querySelector("#botao1")
    botao1.addEventListener("click", e => {
        e.preventDefault()
        showLobinho(novoLoboUm)
    })*/
    
    /*MODIFICNAOD O EXEMPLO DOIS*/
    numeroAleatorio = getRandom(resp.length)
    /*Modificando a imagem*/
    let novoLoboDois = resp[numeroAleatorio]
    let imagemDois = exemploDois.lastElementChild 
    imagemDois.setAttribute('src', novoLoboDois.link_image)

    /*modificando o texto*/
    $.querySelector("#nome2").innerHTML = novoLoboDois.name
    $.querySelector("#idade2").innerHTML = novoLoboDois.age
    $.querySelector("#descrição2").innerHTML = novoLoboDois.description

    /*MODIFICNAOD O EXEMPLO TRES*/
    numeroAleatorio = getRandom(resp.length)
    /*Modificando a imagem*/
    let novoLoboTres = resp[numeroAleatorio]
    let imagemTres = exemploTres.firstElementChild 
    imagemTres.setAttribute('src', novoLoboTres.link_image)

    /*modificando o texto*/
    $.querySelector("#nome3").innerHTML = novoLoboTres.name
    $.querySelector("#idade3").innerHTML = novoLoboTres.age
    $.querySelector("#descrição3").innerHTML = novoLoboTres.description

    /*MODIFICNAOD O EXEMPLO QUATRO*/
    numeroAleatorio = getRandom(resp.length)
    /*Modificando a imagem*/
    let novoLoboQuatro = resp[numeroAleatorio]
    let imagemQuatro = exemploQuatro.lastElementChild 
    imagemQuatro.setAttribute('src', novoLoboQuatro.link_image)

    /*modificando o texto*/
    $.querySelector("#nome4").innerHTML = novoLoboQuatro.name
    $.querySelector("#idade4").innerHTML = novoLoboQuatro.age
    $.querySelector("#descrição4").innerHTML = novoLoboQuatro.description
})