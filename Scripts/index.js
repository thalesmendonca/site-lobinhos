const $ = document

/*Fazendo o header diminuir ao scrollar a pagina*/
let header = $.querySelector("header");
let body = $.querySelector('body');
let distance = 0;

$.addEventListener('scroll', function() {
    var distance = window.pageYOffset;
    if (distance > 100){
      header.classList.remove('large');
      header.classList.add('small');
    } else {
        header.classList.remove('small');
        header.classList.add('large');
    }
});

/*API*/

const url = "https://lobinhos.herokuapp.com/wolves/"


/***
 *    ----    ----   --------     ********   ------------ 
 *    ****    ****  **********   ----------  ************ 
 *    ----    ---- ----    ---- ************ ----         
 *    ************ ***      *** ---  --  --- ************ 
 *    ------------ ---      --- ***  **  *** ------------ 
 *    ****    **** ****    **** ---  --  --- ****         
 *    ----    ----  ----------  ***  **  *** ------------ 
 *    ****    ****   ********   ---      --- ************ 
 *                                                        
 */

/*Exibindo os dois primeiros lobos recebidos da API na seçao exemplos*/

/*Função para conseguir o valor random pra exibição do lobo */
function getRandom(max) {
    return Math.floor(Math.random() * max + 1)
}


fetch(url)
.then(data => data.json())
.then(resp => {
    let exemploUm = $.querySelector(".exUm")
    let exemploDois = $.querySelector(".exDois")
    
    /*MODIFICANOD O EXEMPLO UM*/
    numeroAleatorio = getRandom(resp.length)
    /*Modificando a imagem */
    let novoLoboUm = resp[numeroAleatorio]
    let imagemUm = exemploUm.firstElementChild
    imagemUm.setAttribute('src', novoLoboUm.link_image)
    /*Modificando o texto*/
    $.querySelector("#nome").innerHTML = novoLoboUm.name
    $.querySelector("#idade").innerHTML = novoLoboUm.age
    $.querySelector("#descrição").innerHTML = novoLoboUm.description
    
    
    /*MODIFICNAOD O EXEMPLO DOIS*/
    numeroAleatorio = getRandom(resp.length)
    /*Modificando a imagem*/
    let novoLoboDois = resp[numeroAleatorio]
    let imagemDois = exemploDois.lastElementChild 
    imagemDois.setAttribute('src', novoLoboDois.link_image)

    /*modificando o texto*/
    $.querySelector("#nome2").innerHTML = novoLoboDois.name
    $.querySelector("#idade2").innerHTML = novoLoboDois.age
    $.querySelector("#descrição2").innerHTML = novoLoboDois.description
})
